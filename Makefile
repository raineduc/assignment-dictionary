ASM=nasm
ASMFLAGS=-f elf64
LD=ld
EXECUTABLE=main

.PHONY: all
all: link

main.o: main.asm colon.inc words.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: link
link: main.o lib.o dict.o
	$(LD) -o $(EXECUTABLE) $^

.PHONY: clean
clean:
	rm main *.o
