section .text
%include "lib.inc"

global find_word

find_word:
  test rsi, rsi
  jz .ret_result
  push rsi
  push rdi
  add rsi, 8
  call string_equals
  pop rdi
  pop rsi
  test rax, rax
  jnz .ret_result
  mov rsi, [rsi]
  jmp find_word
.ret_result:
  mov rax, rsi
  ret
