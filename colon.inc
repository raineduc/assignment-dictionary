%ifndef COLON_MACROS
  %define COLON_MACROS
  %define dict_head 0

  %macro colon 2
  %2:
    dq dict_head
    db %1, 0
  %define dict_head %2
  %endmacro
%endif
