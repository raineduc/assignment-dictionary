%include "lib.inc"
extern find_word

global _start

section .data
%include "colon.inc"
%include "words.inc"

prompt_message:
db "Enter the key: ", 0

input_err_message:
db "could not read the word", 0

not_found_message:
db "Nothing was found for the specified key", 0

found_message:
db "Found value: ", 0

section .text

%define BUFFER_SIZE 256

%define exit_err_status 1

_start:
  mov rdi, prompt_message
  call print_string

  sub rsp, BUFFER_SIZE
  mov rdi, rsp
  mov rsi, BUFFER_SIZE
  call read_line
  test rax, rax
  jz input_err
  mov rdi, rax
  mov rsi, dict_head
  call find_word
  test rax, rax
  jz not_found
  add rax, 8
  mov r9, rax
  mov rdi, rax
  call string_length
  inc rax
  add r9, rax
  mov rdi, found_message
  call print_string
  mov rdi, r9
  call print_string
  call print_newline
  call exit
    
not_found:
  mov rdi, not_found_message
  jmp throw_err

input_err:
  mov rdi, input_err_message
  jmp throw_err

throw_err:
  call print_error
  call print_err_newline
  mov rdi, exit_err_status
  call exit

