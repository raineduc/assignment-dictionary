global string_length
global print_string
global print_error
global print_char
global print_newline
global print_err_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
global exit

%define stdin 0
%define stdout 1
%define stderr 2
%define exit_syscall 60
%define write_syscall 1
%define read_syscall 0

%define ascii_minus 0x2D
%define ascii_space 0x20
%define ascii_tab 0x9
%define ascii_newline 0xA
%define ascii_zero 48
%define ascii_nine 57

section .data
minus: db ascii_minus

section .text 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, exit_syscall
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .count
    .end:    
        ret        

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, stdout
    call write_string
    ret

print_error:
    mov rsi, stderr
    call write_string
    ret  

; Принимает код символа и выводит его в stdout
print_char:
    mov rsi, stdout
    call write_char
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ascii_newline
    call print_char
    ret

print_err_newline:
    mov rdi, ascii_newline
    mov rsi, stderr
    call write_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    mov r11, rsp ; save rsp
    dec rsp
    mov byte [rsp], 0 ; null symbol
    .div_loop:
        xor rdx, rdx
        div r10
        add dl, 48 ; to ascii code
        dec rsp
        mov [rsp], dl ; push next number
        cmp rax, 0
        jne .div_loop
    mov rdi, rsp
    push r11
    call print_string
    pop r11
    mov rsp, r11 ; restore rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rax, 0
    jnl .not_neg
    push rdi
    mov rdi, [minus]
    call print_char
    pop rdi
    neg rdi
    .not_neg:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.compare:
    mov al, byte [rdi]
    mov r11b, byte [rsi]
    cmp al, r11b
    je .compare_equal
    mov rax, 0
    ret    
.compare_equal:
    cmp al, 0
    je .compare_end
    inc rdi
    inc rsi
    jmp .compare
.compare_end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rax, read_syscall
    mov rdi, stdin
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rdx, rdx
    cmp rsi, 0
    je .buff_too_small
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, ascii_space
    je read_word
    cmp rax, ascii_tab
    je read_word
    cmp rax, ascii_newline
    je read_word
    cmp rax, 0
    je .word_end
    jmp .write_char
.read_next_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, ascii_space
    je .word_end
    cmp rax, ascii_tab
    je .word_end
    cmp rax, ascii_newline
    je .word_end
    cmp rax, 0
    je .word_end
.write_char:
    inc rdx
    cmp rdx, rsi
    jae .buff_too_small
    mov [rdi + rdx - 1], al
    jmp .read_next_char
.buff_too_small:
    xor rax, rax
    ret
.word_end:
    mov byte [rdi + rdx], 0
    mov rax, rdi
    ret

; Все так же, как и в функции read_word, только пробелы и табуляция не прерывают дальнейшее чтение
read_line:
    xor rax, rax
    xor rdx, rdx
    cmp rsi, 0
    je .buff_too_small
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0xA
    je read_word
    cmp rax, 0
    je .word_end
    jmp .write_char
.read_next_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0xA
    je .word_end
    cmp rax, 0
    je .word_end
.write_char:
    inc rdx
    cmp rdx, rsi
    jae .buff_too_small
    mov [rdi + rdx - 1], al
    jmp .read_next_char
.buff_too_small:
    xor rax, rax
    ret
.word_end:
    mov byte [rdi + rdx], 0
    mov rax, rdi
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r9, r9
    xor r11, r11
    mov r10, 10
.parse_digit:
    mov r11b, byte [rdi + r9]
    cmp r11b, ascii_zero
    jb .parse_uint_end
    cmp r11b, ascii_nine
    ja .parse_uint_end
    sub r11b, ascii_zero
    inc r9
    mul r10
    add rax, r11
    jmp .parse_digit
.parse_uint_end:
    mov rdx, r9
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r11, r11
    mov r11b, byte [rdi]
    cmp r11b, [minus]
    je .parse_int
    call parse_uint
    ret
.parse_int:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .parse_int_end
    inc rdx
    neg rax
.parse_int_end:     
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r11, r11
.copy_loop:
    inc rax
    cmp rax, rdx
    ja .copy_buff_too_small
    mov r11b, byte [rdi + rax - 1]   
    mov [rsi + rax - 1], r11b
    cmp r11b, 0
    je .copy_end
    jmp .copy_loop
.copy_buff_too_small:
    mov rax, 0
.copy_end:
    ret

; Принимает указатель на нуль-терминированную строку и файловый дескриптор
write_string:
    call string_length
    mov rdx, rax
    mov rax, write_syscall
    xchg rsi, rdi
    syscall
    ret

; Принимает код символа и файловый дескриптор
write_char:
    push rdi
    mov rax, write_syscall
    mov rdi, rsi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret